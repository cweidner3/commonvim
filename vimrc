set nocompatible			" be iMproved, required
filetype off				" Reset

"=== VUNDLE Settings Start
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
" CWW: Disabling this since this is a submodule
"Plugin 'VundleVim/Vundle.vim'"

"--- Plugins

" A light and configurable statusline/tabline plugin for Vim
Plugin 'itchyny/lightline.vim'

" The NERDTree is a file system explorer for the Vim editor. Using this
" plugin, users can visually browse complex directory hierarchies, quickly
" open files for reading or editing, and perform basic file system operations
Plugin 'https://github.com/scrooloose/nerdtree'

" Add the Sublime option "Multiple Selection" feature into vim. This allows
" for editing multiple parts of the file at the same time such as renaming a
" local variable.
Plugin 'https://github.com/terryma/vim-multiple-cursors'

" An awesome automatic table creator & formatter allowing one to create neat
" tables as you type.
Plugin 'dhruvasagar/vim-table-mode'

" A Vim plugin which shows a git diff in the 'gutter' (sign column). It shows
" which lines have been added, modified, or removed. You can also preview,
" stage, and undo individual hunks. The plugin also provides a hunk text
" object.
Plugin 'airblade/vim-gitgutter'

" Plugin to include Confluence Wiki syntax highlighting
Plugin 'confluencewiki.vim'

" Syntax highlighting and filetype detection for the 'robot' testing framework.
Plugin 'mfukar/robotframework-vim'

" Asynchronous Linting Engine
Plugin 'w0rp/ale'

" Julia Programming Language
Plugin 'JuliaEditorSupport/julia-vim'

call vundle#end()
"=== VUNDLE Settings End

filetype indent plugin on	" required

" Allow backspacing over everything
set backspace=indent,eol,start
set history=50      " Keep 50 lines of history
set ruler           " Show Cursor position
set showcmd         " display incomplete commands
set incsearch       " do incremental searching

if has('mouse')
    set mouse=a " Use the mouse if it has it
endif

"" Color Scheme Settings
syntax enable
set t_Co=256                    " Set the color options to 16 or 256
"colorscheme vividchalk          " Use Colorscheme
hi Comment ctermfg=Cyan         " Because i wasn't satisified with the previous
                                " colors

set hlsearch        " Use highlighting in searches
set tabstop=4       " Tab is 4 Characters
set shiftwidth=4    " number of spaces to use for autoindenting
set number          " Display line numbers
set noexpandtab     " By default use tabs, this can be overridden in ftplugin
set list            " Whitespace characters are made visible
set lcs=tab:\·\·,trail:#    " Trailing spaces are shown; Also tabs are shown
"set cursorline      " Dislpay highlighting on the row with the cursor
set nowrap          " Do not wrap the text
"set colorcolumn=80  " A column line to reference against
"set textwidth=80    " When using `gq` wrap at column 80, auto-newline
set ignorecase      " If disabled, vim will be strict with case.
set smartcase       " Paired with `ignorecase`, will ignore case only if the
                    " pattern uses only lowercase.
set nojoinspaces    " Disables the two spaces after period when using `gq`
set modeline        " Enables the ability to embed settings into a file.
                    " (i.e. in bash scripts # vim: set expandtab :)
set foldlevel=99    " Level at which folds should be folded when opening files
                    " (setting this so they should not be folded)

" LightLine Plugin Settings
set laststatus=2
set noshowmode
let g:lightline = {
	\ 'colorscheme' : 'powerline',
	\ 'active' : {
	\   'left' : [ [ 'mode', 'paste' ],
	\              [ 'readonly', 'relativepath', 'modified' ] ],
	\   'right' : [[ 'lineinfo' ],
	\              [ 'percent' ],
	\              [ 'fileformat', 'fileencoding', 'filetype' ]]
	\ },
	\ 'inactive' : {
	\   'left' : [ [ 'relativepath' ] ]
	\ },
    \ }

" NERDTree Plugin Settings, press Ctrl-o to open/close it
map <C-o> :NERDTreeToggle<CR>

" Table Mode Plugin settings for ReST-compatible tables.
let g:table_mode_corner='|'
let g:table_mode_corner_corner='|'
let g:table_mode_header_fillchar='-'
" Command for TableMode Emacs Style
command -nargs=0 TMem    let g:table_mode_corner='+' | let g:table_mode_corner_corner='+'
    \ | let g:table_mode_header_fillchar='='
" Command for TableMode Bitbucket Markdown Style
command -nargs=0 TMbb    let g:table_mode_corner='|' | let g:table_mode_corner_corner='|'
    \ | let g:table_mode_header_fillchar='-'

" ALE Settings
let g:ale_linters = {
			\ 'python': ['pylint'],
			\}
let g:ale_python_auto_pipenv = 1
let g:ale_python_pylint_auto_pipenv = 1
let g:ale_python_pylint_use_global = 1

" Diff Algorithms: myers (default), minimal, patience, histogram
" Command to reset to normal alg
command -nargs=0 DiffReset setlocal diffopt=internal,filler,algorithm:myers
" Command to set patience alg
command -nargs=0 DiffAlt setlocal diffopt=internal,filler,algorithm:patience
command -nargs=0 DiffAlt2 setlocal diffopt=internal,filler,algorithm:histogram

" Clear trailing spaces command
command -nargs=0 Cts %s/\s\+$//e

" Already defined above with filetype...
set autoindent " AUTO-Indenting!!!

if !exists(":DiffOrig")
  " Command to show a diff of the unsaved changes
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                  \ | wincmd p | diffthis
endif

