#!/bin/bash

set -e

git checkout .
git pull
git submodule update --init --recursive

