" Vim filetype plugin file
" Language: qml
" Last change: 2014 Feb 8

if exists( 'b:did_ftplugin' )
   finish
endif
let b:did_ftplugin = 1

let s:cpoptions_save = &cpoptions
setlocal cpoptions&vim

" command for undo
let b:undo_ftplugin =
   \ 'let b:browsefilter = "" | ' .
   \ 'setlocal ' .
   \    'comments< '.
   \    'commentstring< ' .
   \    'formatoptions< ' .
   \    'indentexpr<'

if has( 'gui_win32' )
\ && !exists( 'b:browsefilter' )
   let b:browsefilter =
      \ 'qml files (*.qml)\t*.qml\n' .
      \ 'All files (*.*)\t*.*\n'
endif

" Set 'comments' to format dashed lists in comments.
setlocal comments=sO:*\ -,mO:*\ \ ,exO:*/,s1:/*,mb:*,ex:*/,://
setlocal commentstring=//%s

setlocal formatoptions-=t
setlocal formatoptions+=croql

" Spaces instead of tabs!!!
setlocal autoindent
setlocal copyindent
setlocal expandtab       " Use spaces instead of tab
setlocal tabstop=4       " Tab is 4 characters (standard for kernel work is 8)
setlocal shiftwidth=4    " Number of spaces to use for autoindenting
setlocal softtabstop=4   " Backspaces will delete a tab worth of spaces

let &cpoptions = s:cpoptions_save
unlet s:cpoptions_save

" Auto Command to remove trailing Whitespace
autocmd BufWritePre * %s/\s\+$//e

" Custom command for block commenting
if exists(":Comment")
	command -range=% Comment <line1>,<line2>s/^/\/\/
	command -range=% Uncomment <line1>,<line2>s/^\/\//
endif

" EOF
