"setlocal colorcolumn=120  " Draw a line at column n
setlocal expandtab        " Turn tabs into spaces
"setlocal spell            " Enable the spell checker
setlocal textwidth=0      " Do automatically wrap the line

" Folding
setlocal foldmethod=indent " How to determine folds
