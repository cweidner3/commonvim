"setlocal colorcolumn=80  " Display a column line for manual wordwrap
setlocal nowrap          " Do Not word wrap
setlocal autoindent
setlocal copyindent
setlocal number          " Always show line numbers
setlocal shiftround      " Use multiple shiftwidth when indenting with '</>'
setlocal hlsearch        " Highlight search terms

" TAB Settings
setlocal expandtab       " Use spaces instead of tab
setlocal tabstop=4       " Tab is 4 characters (standard for kernel work is 8)
setlocal shiftwidth=4    " Number of spaces to use for autoindenting
setlocal softtabstop=4   " Backspaces will delete a tab worth of spaces

" Folding
setlocal foldmethod=syntax " How to determine folds

" Override some syntax coloring
hi Type        ctermfg=magenta

" Auto Command to remove trailing Whitespace
autocmd BufWritePre * %s/\s\+$//e

" Custom command for block commenting
if exists(":Comment")
	command -range=% Comment <line1>,<line2>s/^/\/\/
	command -range=% Uncomment <line1>,<line2>s/^\/\//
endif

" EOF
