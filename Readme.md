Common Vim Configuration
========================

This repo serves as a vim configuration with a basic set of plugins and settings
to help make using vim better (_such as allowing mouse interaction_).

Installing
----------

To install the configuration, clone the repository to the home directory as
`.vim`. Then update and initialize the submodules. The process is displayed
below.

```
~$ cd "${HOME}"
~$ git clone https://bitbucket.org/cweidner3/commmon.vim .vim
~$ ln -s .vim/vimrc .vimrc
~$ cd .vim
~$ git submodule update --init --recursive
```

By making the `~/.vimrc` a symbolic link, it will update automatically with the
repository.

Updating
--------

To get the latest changes just run the provided bash script, 
`bash ./update-vim-config.sh`.

Search Highlighting
-------------------

When searching a file, the search will be highlighted to help show all
instances that match the search term. To remove the highlight after searching
run the command `:noh` to say no-highlight.

Spelling
--------

Since the spelling will highlight questionable words and this can make reading
the words difficult, spelling has been disabled by default. This can be enabled
with `:set spell`.

To go to the next highlighted word press `]s` (or `[s` for the previous) in
command mode. Then press `z=` to bring up spelling options, press the
appropriate number then `Enter` to correct the spelling. Do not provide a number
and press `Enter` to exit without correcting the word.

To temporarily add a word to the dictionary (_until you exit vim_) press `zG`.
This will clear the highlights for that word by specifying that it is correct.

Custom Commands
---------------

- Clear Trailing Spaces `:Cts`
    - Run this command to remove all trailing spaces (_including tabs_) from the
      current file.

Plugins
-------

- [lightline][]: Provides a nice bar at the bottom which can also provide more
  information than the default bar.
- [nerdtree][]: Allows for you to be able to navigate the filesystem without
  exiting vim.
    - When in command mode, Press `CTRL-o` to toggle the side bar.
    - Press `u` when in the side bar to go up a directory.
    - Press `s` to open a file split vertically next to the current file.
    - Press `i` to open a file split horizontally next to the current file.
- [vim-multiple-cursors][]: Allows for editing multiple places at once.
    - When in command mode, select the text you want to change, and then 
    `Ctrl+N` to create a new cursor on the next text that matches.
    - Press `Ctrl+x` to skip a match.
- [vim-table-mode][]: Aids in the creation of tables by automatically adjusting
  the column widths to match.
    - When in command mode, press `\tm` to toggle table mode.
    - Press `|` to split cells.
    - Press `||` at the beginning of a line to create a horizontal line.
- [vim-gitgutter][]: Shows the git changes in the left hand side.
- [ale][]: (Asynchronous Lint Engine) provides support for using code linters
  with vim and will show errors and warnings in the side bar. Just hover over
  the error to see the message at the bottom of the screen.

Syntax Highlighting Plugins

- [confluencewiki][]: Adds syntax highlighting for confluence files.
- [robotframework-vim][]: Adds syntax highlighting for robot and resource files.
- [julia-vim][]: Syntax highlighting for the Julia Language.

[lightline]: https://github.com/itchyny/lightline.vim
[nerdtree]: https://github.com/scrooloose/nerdtree
[vim-multiple-cursors]: https://github.com/terryma/vim-multiple-cursors
[vim-table-mode]: https://github.com/dhruvasagar/vim-table-mode
[vim-gitgutter]: https://github.com/airblade/vim-gitgutter
[ale]: https://github.com/dense-analysis/ale

[confluencewiki]: https://github.com/vim-scripts/confluencewiki.vim
[robotframework-vim]: https://github.com/mfukar/robotframework-vim
[julia-vim]: https://github.com/JuliaEditorSupport/julia-vim

