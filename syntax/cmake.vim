
set tabstop=4       " Tab is 4 Characters
set shiftwidth=4    " number of spaces to use for autoindenting
set expandtab       " Make the tabs create spaces instead
